$(document).ready(function(){	

//fonction d'affichage du bandeau
function afficheBandeau(statut){
	var texte="";
	var classeAdd="";
	var classRemove="";
	texte = (statut=="success") ? "Opération efféctué avec Succès" : "Echec lors du traitement";
	classeAdd = (statut=="success") ? "alert-success" : "alert-danger";
	classRemove = (statut=="success") ? "alert-danger" : "alert-success";

	$(".bandeau").removeClass(classRemove);
	$(".bandeau").addClass(classeAdd);
	$(".bandeau-texte").html(texte);

	$(".bandeau").show().delay(5000).queue(function(n) {
  		$(this).hide(); n();
	});
}

//action lors du clique sur le like
$(document).on('click','.like',function(event){
  event.preventDefault(); 
  var id_msg = $(this).data('id');
  var old = parseInt($(this).parent().children('span').html());
  var like=old+1;
  $(this).parent().children('span').html(like);
  
  $.ajax({ 
	url:"indexAjax.php?action=aimer", 
	method:"POST",
	data:{id:id_msg, aimer:'aimer'},
	success:function(response){ 
		afficheBandeau("success");
	},
	error:function(){afficheBandeau("error");//swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
	}
  });
});

//action lors de l'envoi d'un message a untilisateur sur son profil
$(document).on('click','#post-submit',function(event){
  event.preventDefault();
   var texte=$("#post-texte").val();
   var image=$("#post-image").val();
   var destinataire=$("#post-destinataire").val();
   $(".post-popup.job_post").removeClass("active");
   $(".wrapper").removeClass("overlay");

  $.ajax({ 
	url:"indexAjax.php?action=messagePrive", 
	method:"POST",
	data:{texte:texte,image:image,destinataire:destinataire,post:"post"},
	success:function(response){ 
		$('.messagePrive').empty();
		$('.messagePrive').html(response);
		afficheBandeau("success");
	},
	error:function(){afficheBandeau("error");//swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
	}
  });
});

//action lors du partage en etant sur le mur
$(document).on('click','.partage-public',function(event){
  event.preventDefault();
  var destinataire=$(".select-destinataire").val();
  var id_message=$("#id_message_partage").val();
  $(".post-popup.post_partage").removeClass("active");
  $(".wrapper").removeClass("overlay");

  $.ajax({ 
	url:"indexAjax.php?action=messagePublic", 
	method:"POST",
	data:{id_message:id_message,destinataire:destinataire},
	success:function(response){ 
		$('.messagePublic').empty();
		$('.messagePublic').html(response);
		afficheBandeau("success");
	},
	error:function(){afficheBandeau("error");//swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
	}
  });
});

//action lors du partage en etant sur le profil utilisateur
$(document).on('click','.partage-prive',function(event){
  event.preventDefault();
   var id_message=$("#id_message_partage").val();
  $(".post-popup.post_partage").removeClass("active");
  $(".wrapper").removeClass("overlay");

  $.ajax({ 
	url:"indexAjax.php?action=messagePrive", 
	method:"POST",
	data:{id_message:id_message,partage:"partage"},
	success:function(response){ 
		$('.messagePrive').empty();
		$('.messagePrive').html(response);
		afficheBandeau("success");
	},
	error:function(){afficheBandeau("error");//swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
	}
  });
});

//action de l'envoi d'un chat dans le chat public
$('#chat-full-submit').on('click',function(event){
  event.preventDefault();
   var texte=$("#chat-full-txt").val();
   var image=$("#chat-full-img").val();

  $.ajax({ 
	url:"indexAjax.php?action=chatList", 
	method:"POST",
	data:{texte:texte,image:image},
	success:function(response){ 
		$("#chat-full-txt").val("");
   		$("#chat-full-img").val("");
		$("#chat-list").empty();
		$("#chat-list").html(response);
	},
	error:function(){afficheBandeau("error"); //swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
	}
  });
});

//recuperer la liste de chat
function loadChats(){
	$.ajax({ 
		url:"indexAjax.php?action=chatBoxList", 
		method:"POST",
		data:{type:"refresh"},
		success:function(response){ 
			$('#chat-box-list').empty();
			$('#chat-box-list').html(response);
		},
		error:function(){afficheBandeau("error"); //swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
		}
    });
    //console.log('rrrrr');
}

//recuperer le nombre de chat 
function getChatCount(){
	$.ajax({ 
		url:"indexAjax.php?action=chatCount", 
		method:"POST",
		dataType: "json",
		success:function(response){ 
			//console.log(response[0].nb);
			$('#nbChats').empty();
			$('#nbChats').html(response[0].nb);
		},
		error:function(){afficheBandeau("error"); //swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
		}
    });
}

//fonction rafraichir chaque 5s le nombre de chat et de messages 
function refreshChat(){
	loadChats();
	getChatCount();
}

// rafraichir chaque 5s le nombre de chat et de messages 
setInterval(refreshChat,5000);

//action de l'envoi d'un chat dans le chat box
$('#chat-box-submit').on('click',function(event){
  event.preventDefault(); 
   var texte=$("#chat-box-txt").val();
   var image=$("#chat-box-img").val();
   $.ajax({ 
		url:"indexAjax.php?action=chatBoxList", 
		method:"POST",
		data:{texte:texte,image:image,type:'add'},
		success:function(response){ 
			$("#chat-box-txt").val("");
	   		$("#chat-box-img").val("");
			$('#chat-box-list').empty();
			$('#chat-box-list').html(response);
		},
		error:function(){afficheBandeau("error"); //swal({title:"", text: " Une erreur est survenu pendant le traitement ! ", type: "error"}); 
		}
    });
});

//action de la modification des parametres utilisateur
$('#parametre-submit').on('click',function(event){
  event.preventDefault();

   var avatar=$("#param-avatar").val();
   var statut=$("#param-statut").val();

  $.ajax({ 
	url:"indexAjax.php?action=updateUser", 
	method:"POST",
	data:{avatar:avatar,statut:statut},
	success:function(response){ 
		$('.photo-profile').attr('src', avatar);
		afficheBandeau("success");
	},
	error:function(){afficheBandeau("error"); 
	}
  });
});


});