#include "Parse.h"

using namespace std;

Parse::Parse(std::String cheminFichier) {
  ifstream fichier(cheminFichier, ios::in);
  if(fichier) {
    this.fichierConverti = fichier;
  }
  else {
    cerr << "Erreur fichier" << endl;
  }
}

Parse::~Parse() {
  this.fichierConverti.close();
}

std::String recupTitre() {
  String titre;
  getline(this.fichierConverti, titre);
  do {
    String titre2;
    getline(this.fichierConverti, titre2);
    if (titre2 = "") continue;
    else if (issupper(titre2[0])) {
      titre += titre2;
      continue;
    }
    break;
  }
  return titre;
}

std::String recupResume() {
  String resume;
  String ligne;
  bool debutResume=false;
  while(getline(this.fichierConverti,ligne)){

    if(!debutResume){
       if(ligne != ""){
      (ligne.substr(0,6)=="Abstract"){
        debutResume=true;
        resume += resume;
      }
    }
    }else{
       resume += resume;
    }

  }

  return resume;
}