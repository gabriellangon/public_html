<?php if(!isset($_SESSION['reqMessage'])){session_start();}?>
                <div class="main-ws-sec">
                 
                  <div class="posts-section">

                    <?php
                     foreach(messageTable::getMessages() as $message) {
                      $post=postTable::getPostById($message->post);
                      $emetteur=utilisateurTable::getUserById($message->emetteur);
                      $destinataire=utilisateurTable::getUserById($message->destinataire);
                      $parent=utilisateurTable::getUserById($message->parent);

                      if(!empty($post) && !empty($emetteur) && !empty($destinataire) ){
                    ?>
                    <div class="post-bar">
                      <div class="post_topbar">
                        <div class="usy-dt">
                          <img width="25" height="25" src="<?=!empty($emetteur[0]['avatar'])?$emetteur[0]['avatar']:'images/avatar-none.jpg';?>" alt="">
                          <div class="usy-name">
                            <h3>
                            <a class="lien" href="?action=profile&id=<?=$emetteur[0]['id'];?>" title="" class="view-more-pro">
                              <?=$emetteur[0]['prenom'].' '.$emetteur[0]['nom'];?>
                             </a>
                             </h3>
                            <span><img src="images/clock.png" alt=""><?=date('j F Y,H:i',strtotime($post[0]['date']));?></span>
                          </div>
                          <img width="25" src="images/chevron-right.png" alt="">
                          <img width="25" height="25" src="<?=!empty($destinataire[0]['avatar'])?$destinataire[0]['avatar']:'images/avatar-none.jpg';?>" alt="">
                          <div class="usy-name">
                          <h3>
                            <a class="lien" href="?action=profile&id=<?=$destinataire[0]['id'];?>" title="" class="view-more-pro">
                              <?=$destinataire[0]['prenom'].' '.$destinataire[0]['nom'];?>
                             </a>
                             </h3>
                              <span style="color:black"> Parent: <a class="lien" href="?action=profile&id=<?=$parent[0]['id'];?>" title="" class="view-more-pro">
                            <?=(!empty($parent[0]['prenom']))?$parent[0]['prenom'].' '.$parent[0]['nom']:'Aucun';?>
                             </a></span>
                          </div>
                        </div>
                      </div>
                      <div class="job_descp">
                        <p><?=$post[0]['texte'];?></p>
                        <?php 
                          if(!empty($post[0]['image'])){
                        ?>
                        <p><img width="100%" src="<?=$post[0]['image'];?>"> </p>
                        <?php
                          }
                        ?>
                      </div>
                      <div class="job-status-bar">
                        <ul class="like-com">
                          <li>
                            <a class="like" data-id="<?=$message->id;?>" ><i class="la la-heart"></i>Aimer</a>
                            <img src="images/liked-img.png" alt="">
                            <span><?=($message->aime!="")?$message->aime:'0';?></span>
                          </li> 
                        </ul>
                        <a class="post-partage" data-id="<?=$message->id?>" ><i class="la la-share"></i>Partager</a>
                      </div>
                    </div><!--post-bar end-->
                  <?php }
                }
                    ?>
                    
                    <div class="process-comm">
                      <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                      </div>
                    </div><!--process-comm end-->

                  </div><!--posts-section end-->
                </div><!--main-ws-sec end-->
