		
		<!--debut include header-->
	      <?php  include("headerSuccess.php");?>
	     <!--fin include header-->

	     <!--debut include Bandeau-->
     	 	<?php  include("bandeau.php");?>
     	 <!--fin include Bandeau-->
	     
	     <section class="cover-sec">
			<img width="1600" height="300" src="images/bg-cover.jpg" alt="">
		</section>

		<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
						<div class="row">
							<div class="col-lg-3">
								<div class="main-left-sidebar">
									<div class="user_profile">
										<div class="user-pro-img">
											<img width="190" width="190" src="<?=(!empty($context->data['user']['avatar']))?$context->data['user']['avatar']:'images/avatar-none.jpg'?>" alt="">
										</div><!--user-pro-img end-->
										<div class="user_pro_status">
											<ul class="flw-status">
												<li>
													<h3 style="font-size:2em" ><?=$context->data['user']['prenom'].' '.$context->data['user']['nom'];?></h3>
													<span><?=date('d-m-Y',strtotime($context->data['user']['date_de_naissance']));?></span>
												</li>
											</ul>
										</div><!--user_pro_status end-->
										<ul class="user-fw-status">
											<li>
											<span><?=$context->data['user']['statut'];?></span>
											</li>
										</ul>
									</div><!--user_profile end-->
					
								</div><!--main-left-sidebar end-->
							</div>
							
								<!--Debut include page message-->
									<div class="col-lg-6 col-md-8 no-pd messagePrive">
							       		 <?php  include("messagePriveSuccess.php");?>
							     	</div>
                                <!--fin include page message-->


								  <!-- Debut include include page liste amis-->
                                         <?php  include("listeAmisSuccess.php");?>
							           
                                   <!-- include include page liste amis-->

						</div>
					</div><!-- main-section-data end-->
				</div> 
			</div>
		</main>

		<div class="post-popup job_post">
			<div class="post-project">
				<h3>Ecrire un Message à <?=$context->data['user']['prenom'].' '.$context->data['user']['nom'];?></h3>
				<div class="post-project-fields">
					<form method="post" action="indexAjax.php?action=message">
						<div class="row">
							<div class="col-lg-12">
								<textarea id="post-texte" name="texte" placeholder="Ecrivez votre message"></textarea>
							</div>
							<div class="col-lg-12">
								<input id="post-image" type=texte name="image" placeholder="Lien de l'image"/>
			<input id="post-destinataire" type="hidden" name="destinataire" value="<?=$context->data['user']['id'];?>" />
							</div>
							<div class="col-lg-12">
								<ul>
						<li><button class="active" id="post-submit" type="submit" value="post" name="post">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div><!--post-project-popup end-->

		<div class="post-popup post_partage">
			<div class="post-project">
				<h3>Partager le message</h3>
				<div class="post-project-fields">
					<form method="post" action="?action=message">
						<div class="row">
							<div class="col-lg-12" id="affiche-partage">
								
							</div>
							<div class="col-lg-2">
								Destinataire
							</div>
							<div class="col-lg-10">
								<select name="destinataire" class="select-destinataire">
									<?php
			                           $req=utilisateurTable::getUsers();
			                           foreach($req as  $listeAmis) {
			          				?>
									<option value="<?=$listeAmis['id'];?>"><?=$listeAmis['prenom'].' '.$listeAmis['nom'];?></option>
									<?php
			                          }
			          				?>
								</select>
								<input type=hidden id="id_message_partage" name="id_message" value="0"/>
								<input type=hidden name="partage" value="partage" />
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active partage-prive" type="submit" value="partage" name="partage">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div><!--post-project-popup end-->

	<!--debut include Chat Box-->
      <?php  include("chatBoxSuccess.php");?>
     <!--fin include Chat Box-->

