
                <div class="main-ws-sec">
                  <div class="post-topbar">
                    <div class="user-picy usy-dt">
                      <img src="<?=(!empty($context->getSessionAttribute('avatar')))?$context->getSessionAttribute('avatar'):'images/avatar-none.jpg'?>" alt="">
                    </div>
                    <div class="post-st">
                      <ul>
                        <li><a class="post-jb active" href="#" title="">Ecrire un Message</a></li>
                      </ul>
                    </div><!--post-st end-->
                  </div><!--post-topbar end-->
                 
                  <div class="posts-section">

                    <?php
                     foreach($context->data['message'] as  $listeMsg) {
                      $destinataire=utilisateurTable::getUserById($listeMsg['emetteur']);
                      $parent=utilisateurTable::getUserById($listeMsg['parent']);
                      if(!empty($destinataire)){
                    ?>

                    <div class="post-bar">
                      <div class="post_topbar">
                        <div class="usy-dt">
                          <img width="45" src="<?=!empty($destinataire[0]['avatar'])?$destinataire[0]['avatar']:'images/avatar-none.jpg';?>" alt="">
                          <div class="usy-name">
                            <h3>
                              <a class="lien" href="?action=profile&id=<?=$destinataire[0]['id'];?>" title="" class="view-more-pro">
                              <?=$destinataire[0]['prenom'].' '.$destinataire[0]['nom'];?>
                                </a>
                              </h3>
                            <span><img src="images/clock.png" alt=""><?=$listeMsg['date'];?></span>
                            <span style="color:black"> Parent: <a class="lien" href="?action=profile&id=<?=$parent[0]['id'];?>" title="" class="view-more-pro">
                              <?=(!empty($parent[0]['prenom']))?$parent[0]['prenom'].' '.$parent[0]['nom']:'Aucun';?>
                             </a></span>
                          </div>
                        </div>
                      </div>
                      <div class="usy-name">
                      </div>
                      <div class="job_descp">
                        <p><?=$listeMsg['texte'];?></p>
                        <?php 
                          if(!empty($listeMsg['image'])){
                        ?>
                        <p><img width="100%" src="<?=$listeMsg['image'];?>"> </p>
                        <?php
                          }
                        ?>
                      </div>
                      <div class="job-status-bar">
                        <ul class="like-com">
                          <li>
                            <a class="like" data-id="<?=$listeMsg['id_message'];?>" ><i class="la la-heart"></i>Aimer</a>
                            <img src="images/liked-img.png" alt="">
                            <span><?=(!empty($listeMsg['aime']))?$listeMsg['aime']:'0';?></span>
                          </li> 
                        </ul>
                        <a class="post-partage" data-id="<?=$listeMsg['id_message'];?>" ><i class="la la-share"></i>Partager</a>
                      </div>
                    </div><!--post-bar end-->
                  <?php
                    }
                    }
                    ?>

                    <div class="process-comm">
                      <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                      </div>
                    </div><!--process-comm end-->
                  </div><!--posts-section end-->
                </div><!--main-ws-sec end-->
        