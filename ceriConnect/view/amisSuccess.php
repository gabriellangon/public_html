		
		<!--debut include header-->
	      <?php  include("headerSuccess.php");?>
	     <!--fin include header-->

		<section class="companies-info">
			<div class="container">
				<div class="company-title">
					<h3>Tous les Amis</h3>
				</div><!--company-title end-->
				<div class="companies-list">
					<div class="row">
						 <?php
                           foreach($context->reqAmis as  $listeAmis) {
          				?>
						<div class="col-lg-3 col-md-4 col-sm-6 col-12">
							<div class="company_profile_info">
								<div class="company-up-info">
									<img width="91px" src="<?=!empty($listeAmis['avatar'])?$listeAmis['avatar']:'images/avatar-none.jpg';?>" alt="">
									<h3><?=$listeAmis['prenom'].' '.$listeAmis['nom'];?></h3>
									<h4><?=date('d/m/Y',strtotime($listeAmis['date_de_naissance']));?></h4>
								</div>
								<a href="?action=profile&id=<?=$listeAmis['id'];?>" title="" class="view-more-pro">Voir Profile</a>
							</div><!--company_profile_info end-->
						</div>
						 <?php } ?>
					</div>
				</div><!--companies-list end-->

				<div class="process-comm">
					<div class="spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
				</div><!--process-comm end-->
			</div>
		</section><!--companies-info end-->

	<!--debut include Chat Box-->
      <!--?php  include("chatBoxSuccess.php");?-->
     <!--fin include Chat Box-->

	

