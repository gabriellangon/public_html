
		<!--debut include header-->
	      <?php  include("headerSuccess.php");?>
	     <!--fin include header-->

	     <!--debut include Bandeau-->
     	 	<?php  include("bandeau.php");?>
     	 <!--fin include Bandeau-->

		<section class="profile-account-setting">
			<div class="container">
				<div class="account-tabs-setting">
					<div class="row">
						<div class="col-lg-3">
							<div class="acc-leftbar">
								<div class="nav nav-tabs" id="nav-tab" role="tablist">
								    <a class="nav-item nav-link" id="nav-deactivate-tab" data-toggle="tab" href="#nav-deactivate" role="tab" aria-controls="nav-deactivate" aria-selected="false"><i class="fa fa-paw"></i>Infos personnelles</a>
								  </div>
							</div><!--acc-leftbar end-->
						</div>
						<div class="col-lg-9">
							<div class="tab-content" id="nav-tabContent">

							  	<div class="tab-pane fade show active" id="nav-acc" role="tabpanel" aria-labelledby="nav-acc-tab">
							  		<div class="acc-setting">
										<form action="?action=updateuser" method="POST">
											<div class="cp-field">
												<h5>Nom</h5>
												<div class="cpp-fiel">
													<input type="text" name="nom" value="<?=$context->getSessionAttribute('nom');?>" readonly>
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cp-field">
												<h5>Prénoms</h5>
												<div class="cpp-fiel">
													<input type="text" name="prenom" value="<?=$context->getSessionAttribute('prenom');?>" readonly>
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cp-field">
												<h5>URL Avatar</h5>
												<div class="cpp-fiel">
													<input type="texte" id="param-avatar" name="avatar" value="<?=$context->getSessionAttribute('avatar');?>">
													<i class="fa fa-globe"></i>
												</div>
											</div>
											<div class="cp-field">
												<h5>Statut</h5>
												<input type="texte" id="param-statut" name="statut" value="<?=$context->getSessionAttribute('statut');?>">
											</div>
											<div class="save-stngs pd3">
												<ul>
													<li><button id="parametre-submit"">Enregistrer</button></li>
													<li><button><a href="?action=parametre" style="color:black;">Restaurer</a></button></li>
												</ul>
											</div><!--save-stngs end-->
										</form>
									</div><!--acc-setting end-->
							  	</div>
							</div>
						</div>
					</div>
				</div><!--account-tabs-setting end-->
			</div>
		</section>


	<!--debut include Chat Box-->
      <?php  include("chatBoxSuccess.php");?>
     <!--fin include Chat Box-->

