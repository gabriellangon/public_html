      <?php
             foreach(chatTable::getChats() as  $chat) {
              $post=postTable::getPostById($chat->post);
              $emetteur=utilisateurTable::getUserById($chat->emetteur);

              if(!empty($post) && !empty($emetteur) ){

                if($emetteur[0]['id']!=$context->getSessionAttribute('user_id')){
            ?>

            <div class="incoming_msg">
              <div class="usy-dt"> <img width="40" src="<?=!empty($emetteur[0]['avatar'])?$emetteur[0]['avatar']:'images/avatar-none.jpg';?>" title="<?=$emetteur[0]['prenom'].' '.$emetteur[0]['nom'];?>" alt=""> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                    <p><?=$post[0]['texte'];?></p>
                    <?php 
                        if(!empty($post[0]['image'])){
                    ?>
                    <p class="incoming_msg_img"><img src="<?=$post[0]['image'];?>" /></p>
                    <?php
                      }
                    ?>
                  <span class="time_date"> <?=$emetteur[0]['prenom'];?>    | <?=date('j F Y,H:i',strtotime($post[0]['date']));?></span>
                </div>
              </div>
            </div>
            <?php 
                  }
                else
                 {
              ?>
            <div class="outgoing_msg">
              <div class="sent_msg">
                    <p><?=$post[0]['texte'];?></p>
                    <?php 
                        if(!empty($post[0]['image'])){
                    ?>
                    <p class="incoming_msg_img"><img src="<?=$post[0]['image'];?>" /></p>
                    <?php
                      }
                    ?>
                <span class="time_date"> <?=date('j F Y,H:i',strtotime($post[0]['date']));?></span> </div>
            </div>

            <?php }
                }
             }
            ?>
