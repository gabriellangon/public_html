
		<!--debut include header-->
	      <?php  include("headerSuccess.php");?>
	     <!--fin include header-->

	     <!--debut include Bandeau de notificatio-->
     	 	<?php  include("bandeau.php");?>
     	 <!--fin include Bandeau notification-->

		<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
						<div class="row">
							     <!--debut include include about-->
							      <?php  include("aboutSuccess.php");?>
							     <!--fin include profil-->

							     <!--Debut include page message de tous les messages-->
							     <div class="col-lg-6 col-md-8 no-pd messagePublic">
							       <?php  include("messagePublicSuccess.php");?>
							     </div>
                                 <!--fin include page message-->

                               <div class="col-lg-3 pd-right-none no-pd logo-avignon">
								<div class="right-sidebar">
									<div class="widget widget-about">
										<img width="100px" src="images/logo.jpg" class="img-responsiv" alt="">
										<h3></h3>
										<span></span>
										<div class="sign_link">
											<h3><a href="#" title="">Partager</a></h3>
											
										</div>
									</div>
									</div>
								</div>	
						</div>
					</div><!-- main-section-data end-->
				</div> 
			</div>
		</main>

		<div class="post-popup post_partage">
			<div class="post-project">
				<h3>Partager le message</h3>
				<div class="post-project-fields">
					<form method="post" action="">
						<div class="row">
							<div class="col-lg-12" id="affiche-partage">
								
							</div>
							<div class="col-lg-2">
								Destinataire
							</div>
							<div class="col-lg-10">
								<select name="destinataire" class="select-destinataire">
									<?php
			                           $req=utilisateurTable::getUsers();
			                           foreach($req as  $listeAmis) {
			          				?>
									<option value="<?=$listeAmis['id'];?>"><?=$listeAmis['prenom'].' '.$listeAmis['nom'];?></option>
									<?php
			                          }
			          				?>
								</select>
								<input type=hidden id="id_message_partage" name="id_message" value="0"/>
								<input type=hidden name="partage" value="partage" />
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active partage-public" value="partage" name="partage">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div><!--post-project-popup end-->

	<!--debut include Chat Box-->
      <?php  include("chatBoxSuccess.php");?>
     <!--fin include Chat Box-->

 
