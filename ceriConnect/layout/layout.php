<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>  
  <title>
     Ton appli !
    </title>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/line-awesome.css">
<link rel="stylesheet" type="text/css" href="css/line-awesome-font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" type="text/css" href="lib/slick/slick.css">
<link rel="stylesheet" type="text/css" href="lib/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="css/jquery.lsxemojipicker.css" >
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--link rel="stylesheet" type="text/css" href="styles/sweetalert.css" /-->  
</head>

  <body>
      <div class="wrapper">
	<?php include($template_view); ?> 
    </div><!--theme-layout end-->
      <?php

        if(!empty($_SESSION['error'])){
            ?>
        	<div class="alert alert-danger wrapper" style="position:absolute; top:50px;text-align: center; ">
             <?=$_SESSION['error'];?> 
           <a data-dismiss="alert" class="close">×</a> 
          </div>
            <?php
            $_SESSION['error']='';
        }
        
      ?>
  
  <form action="?action=message" method="POST" id="form-like">
    <input type="hidden" name="id" value="" id="id_msg"/>
    <input type="hidden" name="aimer" value="aimer" />
</form>

  </body>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/popper.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="lib/slick/slick.min.js"></script>
<script type="text/javascript" src="js/scrollbar.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="js/dispatcher.js"></script>
<script type="text/javascript" src="js/disabled.js"></script>

<!--script>'undefined'=== typeof _trfq || (window._trfq = []);'undefined'=== typeof _trfd && (window._trfd=[]),_trfd.push({'tccl.baseHost':'secureserver.net'}),_trfd.push({'ap':'cpsh'},{'server':'a2plcpnl0235'}) //  </script><script src='../../../img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js'></script-->

</html>
