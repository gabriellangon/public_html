<?php

abstract class basemodel
{
  public $data;

  public function __construct($data=NULL){

    if(!empty($data)){
      foreach($data as $key=>$value){
        $this->$key=$value;
      }
    }
  }

  public function __get($prop)
  {
      return $this->data[$prop];      
  }
    
  public function __set($prop,$value) 
  {
      $this->data[$prop]=$value;      
  }

  public function save()
  {
    $connection = new dbconnection() ;
  
    $sql = "insert into fredouil.".get_class($this)." " ;
    $sql .= "(".implode(",",array_keys($this->data)).") " ;
    $sql .= "values ('".implode("','",array_values($this->data))."')" ;

    $connection->doExec($sql) ;
    $id = $connection->getLastInsertId("fredouil.".get_class($this)) ;

    return $id == false ? NULL : $id ; 
  }

  public function update()
  {
    $connection = new dbconnection() ;

      $sql = "update fredouil.".get_class($this)." set " ;

      $set = array() ;
      foreach($this->data as $att => $value)
        if($att != 'id' && $value)
          $set[] = "$att = '".$value."'" ;

      $sql .= implode(",",$set) ;
      $sql .= " where id=".$this->id ;
  
    $connection->doExec($sql) ;
  }

}
