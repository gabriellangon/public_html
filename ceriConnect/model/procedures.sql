DROP FUNCTION getMessagesSentTo(v_id integer,v_nombre integer); 
CREATE OR REPLACE FUNCTION getMessagesSentTo(v_id integer,v_nombre integer) 
RETURNS SETOF message AS $$
BEGIN
RETURN QUERY SELECT * FROM message WHERE destinataire=v_id ORDER BY id desc LIMIT v_nombre OFFSET 0;
END ;
$$ LANGUAGE plpgsql;

DROP FUNCTION getMessagesByPage(v_debut integer,v_fin integer,v_id integer) ; 
CREATE OR REPLACE FUNCTION getMessagesByPage(v_debut integer,v_fin integer,v_id integer) 
RETURNS SETOF message AS $$
BEGIN
IF(v_id IS NULL) THEN
RETURN QUERY SELECT * FROM message ORDER BY id desc LIMIT v_fin+1-v_debut OFFSET v_debut-1;
ELSE
RETURN QUERY SELECT * FROM message WHERE destinataire=v_id ORDER BY id desc LIMIT v_fin+1-v_debut OFFSET v_debut-1;
END IF;
END ;
$$ LANGUAGE plpgsql;
