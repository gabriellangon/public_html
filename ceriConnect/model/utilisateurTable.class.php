<?php

class utilisateurTable extends basemodel
{
  public static function getUserByLoginAndPass($login,$pass)
  {
    $connection = new dbconnection() ;
    $sql = "select * from fredouil.utilisateur where identifiant='".$login."' and pass='".sha1($pass)."'" ;

    $res = $connection->doQuery( $sql );
    
    if($res === false)
      return false ;

    return $res ;
  }

  public static function getUserById($id)
  {
    $connection = new dbconnection() ;
    $sql = "select * from fredouil.utilisateur where id=".$id;

    $res = $connection->doQuery( $sql );
    
    if($res === false)
      return false ;

    return $res ;
  }

  public static function getUsers()
  {
    $connection = new dbconnection() ;
    $sql = "select * from fredouil.utilisateur order by prenom,nom";

    $res = $connection->doQuery( $sql );
    
    if($res === false)
      return false ;

    return $res ;
  }

  public static function searchUsers($critere)
  {
    $connection = new dbconnection() ;
    $sql = "select prenom,nom from fredouil.utilisateur where prenom ilike '%".$critere."%' OR  nom ilike '%".$critere."%' order by prenom,nom";

    $res = $connection->doQuery( $sql );
    
    if($res === false)
      return false ;

    return $res ;
  }

  public static function get20Users()
  {
    $connection = new dbconnection() ;
    $sql = "select * from fredouil.utilisateur order by prenom,nom limit 20";

    $res = $connection->doQuery( $sql );
    
    if($res === false)
      return false ;

    return $res ;
  }

}
