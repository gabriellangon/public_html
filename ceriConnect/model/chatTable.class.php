<?php 
class chatTable
{
	public static function getChats()
	{
		$connection = new dbconnection();
		$sql = "select * from fredouil.chat order by id desc limit 20";

		$res = $connection->doQueryObject($sql,"chat");
		if($res===false)
			return false;

		return $res;
	}

	public static function getChatsCount()
	{
		$connection = new dbconnection();
		$sql = "select count(*) as nb from fredouil.chat";
		$res = $connection->doQuery($sql);
		if($res===false)
			return false;

		return $res;
	}

	public static function getLastChat()
	{
		$connection = new dbconnection();
		$sql = "select * from chat where id=(select max(id) from chat) ";

		$res = $connection->doQueryObject($sql,"chat");
		if($res===false)
			return false;

		return $res;
	}


}
?>