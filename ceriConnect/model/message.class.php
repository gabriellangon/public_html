<?php 
class message extends basemodel
{
	public static function getPost($id)
	{
		$connection = new dbconnection();
		$sql = "select * from fredouil.message message inner join fredouil.post post on message.post=post.id where message.id=".$id." order by date desc";

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}

	public static function getPostTo($id)
	{
		$connection = new dbconnection();
		$sql = "select message.id id_message,message.emetteur,message.destinataire,message.parent,message.emetteur,message.post, message.aime, post.id  id_post,post.texte,post.date,post.image from fredouil.message message inner join fredouil.post post on message.post=post.id where message.destinataire=".$id." order by message.id desc";

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}

	public static function getEmetteur($id)
	{
		$connection = new dbconnection();
		$sql = "select user.* from fredouil.message message inner join fredouil.utilisateur user on message.emetteur=user.id where message.id=".$id;

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}

	public static function getDestinataire($id)
	{
		$connection = new dbconnection();
		$sql = "select user.* from fredouil.message message inner join fredouil.utilisateur user on message.destinateur=user.id where message.id=".$id;

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}

	public static function getParent($id)
	{
		$connection = new dbconnection();
		$sql = "select user.* from fredouil.message message inner join fredouil.utilisateur user on message.parent=user.id where message.id=".$id;

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}

	public static function getLikes($id)
	{
		$connection = new dbconnection();
		$sql = "select aimer from fredouil.message where message.id=".$id;

		$res = $connection->doQuery($sql);

		if($res===false)
			return false;

		return $res;
	}


}
?>