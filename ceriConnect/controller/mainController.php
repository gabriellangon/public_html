<?php
/*
 * Controler 
 */

class mainController
{

	public static function helloWorld($request,$context)
	{
	    if(empty($_SESSION['user'])){
	        $context->setSessionAttribute('error','Oups! Vous devez etre connecter pour acceder a cette page');
	        return context::ERROR;
	    }
	    
		$context->mavariable="hello world";
		return context::SUCCESS;
	}

	public static function index($request,$context)
	{
		return context::SUCCESS;
	}
	
	//fonction de verification des parametre de connexion
	public static function login($request,$context)
	{
	    if( isset($_POST["login"]) && isset($_POST["password"]) ){
	        $login=$_POST["login"];
	        $pass=$_POST["password"];
	        
	        $user=utilisateurTable::getUserByLoginAndPass($login,$pass);
	        
	        if(!empty($user)){
	            $fullname=$user[0]['prenom'].' '.$user[0]['nom'];
	            $context->setSessionAttribute('user_id',$user[0]['id']);
	            $context->setSessionAttribute('user',$fullname);
	            $context->setSessionAttribute('prenom',$user[0]['prenom']);
	            $context->setSessionAttribute('nom',$user[0]['nom']);
	            $context->setSessionAttribute('avatar',$user[0]['avatar']);
	            $context->setSessionAttribute('statut',$user[0]['statut']);
	            $context->setSessionAttribute('date_de_naissance',$user[0]['date_de_naissance']);
	            $context->setSessionAttribute('msg','Bonjour '.$fullname);
                
	            $context->redirect('././index.php?action=dashboard');
	            //$context->redirect($_SERVER["HTTP_REFERER"]);
	        }else{
	            $context->setSessionAttribute('error','Oups! Login ou mot de passe incorrecte');
	        }
	    }else{
	    	if(!empty($_SESSION['user'])){$_SESSION['user']="";}
	    }
	    
	    return context::SUCCESS;
	}
	
	//affichage du mur et des messages
	public static function dashboard($request,$context)
	{	
		$context->setSessionAttribute('reqMessage',messageTable::getMessages());
		//$context->reqMessage=messageTable::getMessages();

	    return context::SUCCESS;
	}

	//affichage du profile et des messages addréssé à l'utilisateur 
	public static function profile($request,$context)
	{
		$id=(!empty($request['id']))?$request['id']:$context->getSessionAttribute('user_id');

		 $user= utilisateurTable::getUserById($id); 
		 $message=message::getPostTo($id);

          $data['user']=$user[0];
          $data['message']=$message;
          $context->data=$data;
      
	    return context::SUCCESS;
	}

	//affichage de la liste d'amis
	public static function amis($request,$context)
	{	
		$context->reqAmis=utilisateurTable::getUsers();
	    return context::SUCCESS;
	}

	//affichage du chat oublic
	public static function chat($request,$context)
	{	
		$context->reqChat=chatTable::getChats();
	    return context::SUCCESS;
	}
	//affichage parametre du compte
	public static function parametre($request,$context)
	{
	    return context::SUCCESS;
	}
	//fonction recherche dans la liste d'amis
	public static function recherche($request,$context)
	{	
		$context->json=array();
		foreach(utilisateurTable::searchUsers($request['critere']) as $user){
			$nom_complet = $ami["prenom"]." ".$ami["nom"];
			array_push($context->json, nom_complet);
		}
		
	    return context::NONE;
	}

	//affichage de tous les messages
	public static function messagePublic($request,$context)
	{
		$messages = messageTable::getMessageById($request['id_message']);
		$message=$messages[0];
		$partage['parent']=$message->parent;
		$partage['emetteur']=$context->getSessionAttribute('user_id');
		$partage['destinataire']=$request['destinataire'];
		$partage['post']=$message->post;
		
		$partage = new message($partage);
		$idMesssage=$partage->save();
		
		return context::SUCCESS;
	}

	//affichage des messages adressé à l'utilisateur 
	public static function messagePrive($request,$context)
	{
		if(!empty($request['partage'])){
			$messages = messageTable::getMessageById($request['id_message']);
			$message=$messages[0];
			$partage['parent']=$message->parent;
			$partage['emetteur']=$context->getSessionAttribute('user_id');
			$partage['destinataire']=$context->getSessionAttribute('user_id');
			$partage['post']=$message->post;

			$partage = new message($partage);
			$idMesssage=$partage->save();

			$user=utilisateurTable::getUserById($context->getSessionAttribute('user_id')); 
			$message=message::getPostTo($context->getSessionAttribute('user_id'));

	        $data['user']=$user[0];
	        $data['message']=$message;
	        $context->data=$data;

	        return context::SUCCESS;
		}else if(!empty($request['post'])){
			$image = (empty($request['image']) ? "null" : $request['image']);
			$postmessage['texte'] = $request['texte'];
			$postmessage['image']=$request['image'];
			$postmessage['date']= date("Y-m-d H:i:s");
			$post = new post($postmessage);
			$id=$post->save();
			
			$messageInfo['emetteur'] = $context->getSessionAttribute('user_id');
			$messageInfo['destinataire']=$request['destinataire'];
			$messageInfo['parent']=$context->getSessionAttribute('user_id');
			$messageInfo['post']=$id;

			$message =new message($messageInfo);
			$idm=$message->save();

			$user= utilisateurTable::getUserById($request['destinataire']); 
		 	$message=message::getPostTo($request['destinataire']);

          	$data['user']=$user[0];
          	$data['message']=$message;
          	$context->data=$data;

			return context::SUCCESS;
		}
		

		return context::SUCCESS;
	}

	//fonction pour ajout de like
	public static function aimer($request,$context)
	{
		$messages= messageTable::getMessageById($request['id']);
		$message=$messages[0];

		$message2['id']=$message->id;
		$message2['aime']=$message->aime+1;
		$message2 =new message($message2);

		$message2->update();

		$context->setSessionAttribute('reqMessage',messageTable::getMessages());

		return context::NONE;
	}

	//fonction de modifications des parametre utilisateur
	public static function updateUser($request,$context)
	{
		$users=utilisateurTable::getUserById($context->getSessionAttribute('user_id'));
		$user=$users[0];

		$user2['id']=$user['id'];
		$user2['avatar']=$request['avatar'];
		$user2['statut']=$request['statut'];

		$user2 = new utilisateur($user2);
		$user2->update();
		$context->setSessionAttribute('avatar',$user2->avatar);
        $context->setSessionAttribute('statut',$user2->statut);
    	
    	return context::NONE;
	}

	//recuperation des chats dans le chat public
	public static function chatList($request,$context)
	{
		$post['texte']=$request['texte'];
		$post['image']=$request['image'];
		$post['date']= date("Y-m-d H:i:s");
		$post = new post($post);
		$id=$post->save();

        $chat['post']=$id;
        $chat['emetteur']=$context->getSessionAttribute('user_id');
		$chat=new chat($chat);
		$idchat=$chat->save();

		return context::SUCCESS;
	}

	//recuperation des chats dans le chat box
	public static function chatBoxList($request,$context)
	{
		if($request['type']=="add"){
			$post['texte']=$request['texte'];
			$post['image']=$request['image'];
			$post['date']= date("Y-m-d H:i:s");
			$post = new post($post);
			$id=$post->save();

	        $chat['post']=$id;
	        $chat['emetteur']=$context->getSessionAttribute('user_id');
			$chat=new chat($chat);
			$idchat=$chat->save();
		}
		
		return context::SUCCESS;
	}

	//recuperation du nombre de message
	public static function chatCount($request,$context)
	{
		$context->json=chatTable::getChatsCount();
		return context::NONE;
	}

	//fonction avant ajax like, post ,chat
	public static function message($request,$context)
	{

		if(!empty($request['post'])) {
			$image = (empty($request['image']) ? "null" : $request['image']);
			$postmessage['texte'] = $request['texte'];
			$postmessage['image']=$request['image'];
			$postmessage['date']= date("Y-m-d H:i:s");
			$post = new post($postmessage);
			$id=$post->save();
			
			$messageInfo['emetteur'] = $context->getSessionAttribute('user_id');
			$messageInfo['destinataire']=$request['destinataire'];
			$messageInfo['parent']=$context->getSessionAttribute('user_id');
			$messageInfo['post']=$id;

			print_r($messageInfo);
			$message =new message($messageInfo);
			$idm=$message->save();
			
			if(!empty($idm)) {
				context::redirect($_SERVER["HTTP_REFERER"]);
			}
			else {
				context::redirect("././index.php?action=dashboard&msg=".$idm."");
			}
			
		}
		else if(!empty($request['aimer'])) {
			
			$messages= messageTable::getMessageById($request['id']);
			$message=$messages[0];

			$message2['id']=$message->id;
			$message2['aime']=$message->aime+1;
			$message2 =new message($message2);
			$message2->save();

			//return context::SUCCESS;
			context::redirect($_SERVER["HTTP_REFERER"]);

		}
		else if(!empty($request['chat'])) {
			$post['texte']=$request['texte'];
			$post['image']=$request['image'];
			$post['date']= date("Y-m-d H:i:s");
			print_r($post);
			$post = new post($post);
			$id=$post->save();

            $chat['post']=$id;
            $chat['emetteur']=$context->getSessionAttribute('user_id');
			$chat=new chat($chat);
			print_r($chat);
			$idchat=$chat->save();

			if(!empty($idchat)) {
	        context::redirect($_SERVER["HTTP_REFERER"]);
			}
			else {
				context::redirect("././index.php?action=dashboard");
			}
			
		}else if(!empty($request['partage'])){
			//echo $request['id_message'];
			$messages = messageTable::getMessageById($request['id_message']);
			$message=$messages[0];
			$partage['parent']=$message->parent;
			$partage['emetteur']=$context->getSessionAttribute('user_id');
			$partage['destinataire']=$request['destinataire'];
			$partage['post']=$message->post;

			$partage = new message($partage);
			$idMesssage=$partage->save();

			if(!empty($idMesssage)) {
	        context::redirect($_SERVER["HTTP_REFERER"]);
			}
			else {
				context::redirect("././index.php?action=dashboard");
			}
		}

		//return context::ERROR;
	}

	
	
}
