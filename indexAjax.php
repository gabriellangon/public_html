<?php
//nom de l'application
$nameApp = "ceriConnect";

//action par défaut
$action = "login";

if(key_exists("action", $_REQUEST))
$action =  $_REQUEST['action'];

require_once 'lib/core.php';
require_once $nameApp.'/controller/mainController.php';
session_start();
$context = context::getInstance();
$context->init($nameApp);

$view=$context->executeAction($action, $_REQUEST);
$data=array();
//traitement des erreurs de bases, reste a traiter les erreurs d'inclusion
if($view===false)
{	
	echo 'erreur';
	//$data['status']='false';
	//$data['texte']="Une grave erreur s'est produite, il est probable que l'action ".$action." n'existe pas...";
	die;
}
//afficher les données returné
elseif($view==context::NONE)
{
	if(!empty($context->json)){

		if(is_array($context->json)){
			foreach($context->json as $ami){
			$nom_complet = $ami["prenom"]." ".$ami["nom"];
			$data[]=$nom_complet;
			}
		}else{
			$data=$context->json;
		}
		
	}

	header("Content-type:application/json");
	echo json_encode($context->json);


}
elseif($view!=context::NONE)
{
	//$data['status']='true';
	//$data['texte']=$nameApp."/view/".$action.$view.".php";
	include($nameApp."/view/".$action.$view.".php");
}


?>
