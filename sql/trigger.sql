/*Retourne les n derniers messages*/
create or replace function getMessagesSentTo(id_user integer, nombre integer)
RETURNS void as $$
DECLARE
liste_msg CURSOR IS SELECT * FROM message WHERE destinataire=id_user ORDER BY id desc LIMIT 0,nombre ;
BEGIN
RETURN liste_msg;
END;
$$ LANGUAGE 'plpgsql';
